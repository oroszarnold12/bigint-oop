//Nev:Orosz Arnold- Daniel
//Csoport:513
//Azonosito:oaim1856

#include <iostream>
#include "NagyEgesz.h"

using namespace std ;

int main()
{
    int a[4]={1,2,3,4} ;
    int b[4]={1,2,3,4} ;
    NagyEgesz A(1,4,a) ;
    A.kiir() ;
    NagyEgesz B(-1,4,b) ;
    B.kiir() ;
    A.osszead(B).kiir() ;
    A.kivon(B).kiir() ;
    A.szoroz(B).kiir() ;
    A.oszt(B).kiir() ;
    NagyEgesz alap ;
    alap.kiir() ;
    NagyEgesz masolo(A) ;
    masolo.kiir() ;
}
