//Nev:Orosz Arnold- Daniel
//Csoport:513
//Azonosito:oaim1856

class NagyEgesz
{
    int *szam ;
    int hossz ;
    int jel ;
    bool nagyobb(const NagyEgesz& , const NagyEgesz&) ;
    void torolnulla(NagyEgesz&) ;
public:
    NagyEgesz() ;
    NagyEgesz(int , int , const int*) ;
    NagyEgesz(const NagyEgesz&) ;
    ~NagyEgesz() ;
    class NullavalValoOsztas{} ;
    NagyEgesz osszead(const NagyEgesz&) ;
    NagyEgesz kivon(const NagyEgesz&) ;
    NagyEgesz szoroz(const NagyEgesz&) ;
    NagyEgesz oszt(const NagyEgesz&) ;
    void kiir() const ;
    NagyEgesz& operator =(const NagyEgesz&);
};
