//Nev:Orosz Arnold- Daniel
//Csoport:513
//Azonosito:oaim1856

#include "NagyEgesz.h"
#include <iostream>

using namespace std ;

NagyEgesz::NagyEgesz()
{
    hossz=1 ;
    szam=new int[hossz] ;
    szam[0]=0 ;
    jel=0 ;
}

NagyEgesz::NagyEgesz(int elojel , int n , const int* szamjegyek)
{
    jel=elojel ;
    hossz=n ;
    szam=new int[hossz] ;
    for(int i=0; i<hossz; i++)
    {
        szam[i]=szamjegyek[i] ;
    }
}

NagyEgesz::NagyEgesz(const NagyEgesz& X)
{
    jel=X.jel ;
    hossz=X.hossz ;
    szam=new int[hossz] ;
    for(int i=0; i<hossz; i++)
    {
        szam[i]=X.szam[i] ;
    }
}

NagyEgesz::~NagyEgesz()
{
    delete [] szam ;
}

NagyEgesz NagyEgesz::osszead(const NagyEgesz& X)
{
    if(jel==X.jel)
    {
        int rhossz , nhossz ;
        int *hosszabb , *rovidebb ;
        if(nagyobb(NagyEgesz(1,hossz,szam),NagyEgesz(1,X.hossz,X.szam)))
        {
            rhossz=X.hossz ;
            nhossz=hossz ;
            hosszabb=new int[nhossz+1] ;
            for(int i=0; i<hossz; i++)
            {
                hosszabb[i]=szam[hossz-i-1] ;
            }
            hosszabb[nhossz]=0 ;
            rovidebb=new int[X.hossz] ;
            for(int i=0; i<X.hossz; i++)
            {
                rovidebb[i]=X.szam[X.hossz-i-1] ;
            }
        }
        else
        {
            rhossz=hossz ;
            nhossz=X.hossz ;
            hosszabb=new int[nhossz+1] ;
            for(int i=0; i<X.hossz; i++)
            {
                hosszabb[i]=X.szam[X.hossz-i-1] ;
            }
            hosszabb[nhossz]=0 ;
            rovidebb=new int[hossz] ;
            for(int i=0; i<hossz; i++)
            {
                rovidebb[i]=szam[hossz-i-1] ;
            }
        }
        int carry=0 ;
        int i ;
        for(i=0; i<rhossz; i++)
        {
            hosszabb[i]+=rovidebb[i] ;
            hosszabb[i]+=carry ;
            carry=hosszabb[i]/10 ;
            hosszabb[i]=hosszabb[i]%10 ;
        }
        if(carry==1)
        {
			hosszabb[i]+=carry ;
		}
        if(hosszabb[nhossz]!=0)
        {
            int *eredmeny ;
            eredmeny=new int[nhossz+1] ;
            for(int i=0; i<nhossz+1; i++)
            {
                eredmeny[i]=hosszabb[nhossz-i] ;
            }
            NagyEgesz Z(jel,nhossz+1,eredmeny) ;
            return Z ;
            delete [] eredmeny ;
        }
        else
        {
            int *eredmeny ;
            eredmeny=new int[nhossz] ;
            for(int i=0; i<nhossz; i++)
            {
                eredmeny[i]=hosszabb[nhossz-i-1] ;
            }
            NagyEgesz Z(jel,nhossz,eredmeny) ;
            return Z ;
            delete [] eredmeny ;
        }
        delete [] hosszabb ;
        delete [] rovidebb ;
    }
    else
    {
        int ideigjel , ideighossz , *eredmeny ;
        if(nagyobb(NagyEgesz(jel,hossz,szam),X))
        {
            ideigjel=jel ;
            ideighossz=hossz ;
            eredmeny=new int[ideighossz] ;
            for(int i=0; i<ideighossz; i++)
            {
                eredmeny[i]=szam[hossz-i-1] ;
            }
            int *kisebb ;
            kisebb=new int[X.hossz] ;
            for(int i=0; i<X.hossz; i++)
            {
                kisebb[i]=X.szam[X.hossz-i-1] ;
            }
            for(int i=0; i<X.hossz; i++)
            {
                eredmeny[i]-=kisebb[i] ;
                if(eredmeny[i]<0)
                {
                    eredmeny[i+1]-- ;
                    eredmeny[i]+=10 ;
                }
            }
            while(eredmeny[ideighossz-1]==0&&ideighossz>1)
            {
                ideighossz-- ;
            }
            int *veredmeny ;
            veredmeny=new int[ideighossz] ;
            for(int i=0; i<ideighossz; i++)
            {
                veredmeny[i]=eredmeny[ideighossz-i-1] ;
            }
            NagyEgesz Z(ideigjel,ideighossz,veredmeny) ;
            delete [] eredmeny ;
            delete [] kisebb ;
            delete [] veredmeny ;
            return Z ;
        }
        else
        {
            ideigjel=X.jel ;
            ideighossz=X.hossz ;
            eredmeny=new int[ideighossz] ;
            for(int i=0; i<ideighossz; i++)
            {
                eredmeny[i]=X.szam[X.hossz-i-1] ;
            }
            int *kisebb ;
            kisebb=new int[hossz] ;
            for(int i=0; i<hossz; i++)
            {
                kisebb[i]=szam[hossz-i-1] ;
            }
            for(int i=0; i<hossz; i++)
            {
                eredmeny[i]-=kisebb[i] ;
                if(eredmeny[i]<0)
                {
                    eredmeny[i+1]-- ;
                    eredmeny[i]+=10 ;
                }
            }
            while(eredmeny[ideighossz-1]==0&&ideighossz>1)
            {
                ideighossz-- ;
            }
            int *veredmeny ;
            veredmeny=new int[ideighossz] ;
            for(int i=0; i<ideighossz; i++)
            {
                veredmeny[i]=eredmeny[ideighossz-i-1] ;
            }
            NagyEgesz Z(ideigjel,ideighossz,veredmeny) ;
            delete [] eredmeny ;
            delete [] kisebb ;
            delete [] veredmeny ;
            return Z ;
        }
    }
}

NagyEgesz NagyEgesz::kivon(const NagyEgesz& X)
{
    NagyEgesz Y(-X.jel,X.hossz,X.szam) ;
    return this->osszead(Y) ;
}

NagyEgesz NagyEgesz::szoroz(const NagyEgesz &X)
{
    int ideighossz=hossz+X.hossz , ideigjel ;
    if(jel==X.jel)
    {
        ideigjel=+1 ;
    }
    else
    {
        ideigjel=-1 ;
    }
    int *ideigszam ;
    ideigszam=new int[ideighossz] ;
    int *elso , *masodik ;
    elso=new int[hossz] ;
    masodik=new int[X.hossz] ;
    for(int i=0;i<hossz;i++)
    {
        elso[i]=szam[hossz-i-1] ;
    }
    for(int i=0;i<X.hossz;i++)
    {
        masodik[i]=X.szam[X.hossz-i-1] ;
    }
    for(int i=0;i<ideighossz;i++)
    {
        ideigszam[i]=0 ;
    }
    int carry=0 ;
    int i , j ;
    for(i=0;i<hossz;i++)
    {
        for(j=0;j<X.hossz;j++)
        {
            ideigszam[i+j]+=elso[i]*masodik[j] ;
			if (ideigszam[i+j]>9)
            {
				carry=ideigszam[i+j]/10 ;
				ideigszam[i+j]=ideigszam[i+j]%10 ;
			}
			else
			{
				carry=0 ;
			}
			ideigszam[i+j+1]+=carry ;
        }
    }
    while(ideigszam[ideighossz-1]==0&&ideighossz>1)
    {
        ideighossz-- ;
    }
    int *eredmeny ;
    eredmeny=new int[ideighossz] ;
    for(int i=0;i<ideighossz;i++)
    {
        eredmeny[i]=ideigszam[ideighossz-i-1] ;
    }
    NagyEgesz Z(ideigjel,ideighossz,eredmeny) ;
    delete [] ideigszam ;
    delete [] elso ;
    delete [] masodik ;
    delete [] eredmeny ;
    return Z ;
}

NagyEgesz NagyEgesz::oszt(const NagyEgesz &X)
{
    if(X.szam[0]==0)
    {
        throw NullavalValoOsztas() ;
    }
    NagyEgesz elso(1,hossz,szam) ;
    NagyEgesz masodik(1,X.hossz,X.szam) ;
	bool onagyobb ;
	if(nagyobb(elso,masodik))
    {
        onagyobb=true ;
    }
	else
    {
        onagyobb=false ;
    }
	if (!onagyobb)
    {
		return NagyEgesz() ;
	}
	int* eredmeny ;
	int index=0 ;
	eredmeny=new int[elso.hossz] ;
	for(int i=0;i<elso.hossz;i++)
	{
	    eredmeny[i]=0 ;
	}
	int* seged ;
	NagyEgesz Seged ;
	int ciklus=X.hossz ;
	while(onagyobb||ciklus<=hossz)
    {
		seged=new int[elso.hossz] ;
		for(int i=0;i<masodik.hossz;i++)
		{
		    seged[i]=masodik.szam[i] ;
		}
		for(int i=masodik.hossz;i<elso.hossz;i++)
        {
            seged[i]=0 ;
        }
		if(elso.hossz==Seged.hossz)
        {
			Seged=NagyEgesz(1,elso.hossz-1,seged) ;
		}
		else
		{
			Seged=NagyEgesz(1,elso.hossz,seged) ;
		}
		delete [] seged ;
		while(nagyobb(elso,Seged))
		{
			eredmeny[index]++ ;
			elso=elso.kivon(Seged) ;
		}
		index++ ;
		torolnulla(elso) ;
		if(nagyobb(elso,masodik))
        {
            onagyobb=true ;
        }
		else
        {
            onagyobb=false ;
        }
		ciklus++;
	}
	NagyEgesz Z(jel*X.jel,index,eredmeny) ;
	torolnulla(Z) ;
	delete[] eredmeny;
	return Z;
}

void NagyEgesz::kiir() const
{
    if(jel==1)
    {
        cout << "+" ;
    }
    if(jel==-1)
    {
        cout << "-" ;
    }
    for(int i=0; i<hossz; i++)
    {
        cout << szam[i] ;
    }
    cout << endl ;
}

NagyEgesz& NagyEgesz::operator =(const NagyEgesz &X)
{
	if(this!=&X)
    {
		delete [] szam ;
		jel=X.jel ;
		hossz=X.hossz ;
		szam=new int[hossz] ;
		for(int i=0;i<hossz;i++)
		{
		    szam[i]=X.szam[i] ;
		}
	}
	return *this ;
}

bool NagyEgesz::nagyobb(const NagyEgesz& X, const NagyEgesz& Y)
{
    if(X.hossz>Y.hossz)
    {
        return true ;
    }
    else
    {
        if(X.hossz<Y.hossz)
        {
            return false ;
        }
        else
        {
            int i=0 ;
            while(i<X.hossz)
            {
                if(X.szam[i]>Y.szam[i])
                {
                    return true ;
                }
                else
                {
                    if(X.szam[i]<Y.szam[i])
                    {
                        return false ;
                    }
                }
                i++ ;
            }
            return true ;
        }
    }
}
void NagyEgesz::torolnulla(NagyEgesz &X)
{
	if(X.szam[0]==0)
    {
		int i=0 ;
		while(X.szam[i]==0&&i<X.hossz)
		{
			i++ ;
		}
		if(i==X.hossz)
		{
			delete[] X.szam ;
			X.hossz=1 ;
            X.jel=1 ;
			X.szam=new int[1] ;
			X.szam[0]=0 ;
		}
		else
		{
			int* seged ;
			seged=new int[X.hossz-i] ;
			for(int j=i;j<X.hossz;j++)
			{
			    seged[j-i]=X.szam[j] ;
			}
			delete [] X.szam ;
			X.hossz=X.hossz-i ;
			X.szam=new int[X.hossz] ;
			for(int j=0;j<X.hossz;j++)
            {
                X.szam[j]=seged[j] ;
            }
			delete [] seged;
		}
	}
}
